package com.example.td1_13;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.widget.LinearLayout.VERTICAL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);




        LinearLayout info = new LinearLayout(this);
        info.setId(R.id.info);
        info.setOrientation(VERTICAL);

        TextView text_view_id = new TextView(this);
        text_view_id.setText("Your text");
        info.addView(text_view_id);

        EditText editText1 = new EditText(this);
        editText1.setEms(10);
        editText1.setHint("Enter a text");


        info.addView(editText1);

        Button button = new Button(this);
        button.setText("Set Text");
        info.addView(button);
        setContentView(info);

       /* LinearLayout layout = (LinearLayout)findViewById(R.id.info);
        TextView tv =(TextView)findViewById(R.id.text_view_id);
        EditText mEdit=(EditText)findViewById(R.id.editText1);
        Button button = (Button) findViewById(R.id.button);*/
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                text_view_id.setText(editText1.getText().toString());
            }
        });
    }
}